<?php

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/get/product', [ProductController::class, 'get'])->name('product.get');
Route::post('/create/product', [ProductController::class, 'create'])->name('product.create');
Route::post('/update/product', [ProductController::class, 'update'])->name('product.edit');
Route::post('/delete/product', [ProductController::class, 'delete'])->name('product.delete');
