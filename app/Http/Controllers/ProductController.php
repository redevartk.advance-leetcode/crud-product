<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    private $productRepository;

    public function __construct(ProductRepository $productRepository){
        $this->productRepository = $productRepository;
    }

    public function get(Request $request){
        try {
            $result = $this->productRepository->get($request);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => $result, 'message' => 'failed'], 500);
        }
    }

    public function create(Request $request){
        try {
            $result = $this->productRepository->create($request);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => $result, 'message' => 'failed'], 500);
        }
    }

    public function update(Request $request){
        try {
            $result = $this->productRepository->update($request['id'], $request);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => $result, 'message' => 'failed'], 500);
        }
    }

    public function delete(Request $request){
        try {
            $result = $this->productRepository->delete($request['id']);
            return response()->json(['data' => $result, 'message' => 'Success'], 200);
        } catch (\Throwable $th) {
            return response()->json(['data' => $result, 'message' => 'failed'], 500);
        }
    }


}
