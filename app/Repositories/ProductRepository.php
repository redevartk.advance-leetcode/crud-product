<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository{

    private $productModel;

    public function __construct(){
        $this->productModel = new Product();
    }

    public function get($data = null){

        if(!empty($data) && !empty($data['field'])){
            $query = $this->productModel->where($data['field']['header'], $data['field']['body'])->orderByDesc('created_at');
        } else {
            $query = $this->productModel->orderByDesc('created_at');
        }

        if(!empty($data) && !empty($data['pagination'])){
            $result = $query->paginate($data['pagination']);
        } else {
            $result = $query->get();
        }

        return $result;
    }

    public function create($data){
        $result = $this->productModel->create([
            'nama' => $data['nama'],
            'deskripsi' => $data['deskripsi'],
            'harga' => $data['harga'],
            'kategori' => $data['kategori'],
        ]);
        return $result;
    }

    public function update($id, $data){
        unset($data['id']);
        $result = $this->productModel->where('id', $id)->update($data->all());
        return $result;
    }

    public function delete($id){
        $result = $this->productModel->where('id', $id)->delete();
        return $result;
    }
}
